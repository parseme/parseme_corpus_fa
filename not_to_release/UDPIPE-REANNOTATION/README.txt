This folder contains working files for morphosyntactic re-annotation of Farsi release 1.3.

Agata Savary

8 Feb 2023

The contents of the folder:

- in (dev|train|test).udpipe-2.10-lemma-to-deprel.cupt files, the tokenization is kept from the previous corpus (1.1), but columns LEMMA to DEP_REL are regenerated automatically using UDPipe with model  persian-perdt-ud-2.10-220711

- in (dev|train|test).udpipe-2.10-head-deprel.cupt, all columns from the 1.1 are kep except HEAD and DEPREL, which are regenerated automatically like above

- in (dev|train|tes).udpipe-2.10-xpos-to-deprel.cupt, columns ID to UPOS are kept from 1.1, while XPOS to DEP_REL are regenerated.

The Farsi team chose version xpos-to-deprel.cupt. It is promoted to the root of the repo.
