README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Farsi (aka Persian), edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Farsi data result from an update and an extension of the Farsi part of the [PARSEME 1.1 corpus](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to the 1.1 version, see the change log below.


Corpora
-------
All the annotated data come from a subset of the Farsi section of the MULTEXT-East "1984" annotated corpus 4.0 (for further information, see https://www.clarin.si/repository/xmlui/handle/11356/1043).
See the change log for adapatations of this corpus.

Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Manually annotated.
* UPOS (column 4): Available. Automatically converted from the manually annotated MULTEXT tagset. The tagset is the [Universal POS-tags](http://universaldependencies.org/u/pos).
* XPOS (column 5): Available. Automatically generated (UDPipe 2, model persian-perdt-ud-2.10-220711). 
* FEATS (column 6): Available. Automatically generated (UDPipe 2, model persian-perdt-ud-2.10-220711). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically generated (UDPipe 2, model persian-perdt-ud-2.10-220711). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: LVC.full, VID.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 


Tokenization, Transliteration, and Encoding
-------------------------------------------
Manual tokenization is performed according to the guidelines set for the tokenization of the Farsi section of the MULTEXT-East "1984" [1], which implements recommendations by Iran's Academy of Farsi Language and Literature. Accordingly, where necessary, the zero-width non-joiner (ZWNJ) -- Unicode character 0x200C -- is used.

Short vowels are not transliterated in the corpus except for the ezafe markers at the end of words (i.e., Unicode character 0x0650).

The data are encoded in UTF-8. 


Authors
----------
All VMWEs annotations were performed by Behrang Qasemizadeh.


License
----------
This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (http://creativecommons.org/licenses/by-nc-sa/4.0/). 

The Farsi section of the MULTEXT-East "1984" annotated corpus 4.0, of which this data set is originated from, is published under MULTEXT-East licence: Freely available for non-commercial use with permission of Behrang QasemiZadeh, and provided that this Header is included in its entirety with any copy distributed.

Contact
----------
* sarlak3@gmail.com
* yalda.yarandi@gmail.com
* m-shams@sbu.ac.ir

[1] Qasemizadeh and Rahimi. Persian in MULTEXT-East Framework. FinTAL 2006, DOI: 10.1007/11816508_54.
[2] Iran's Academy of Farsi Language and Literature. Official Farsi Orthography. ISBN: 964-7531-13-3, 3rd Edition, 2005. 

Future work
-----------
* Version 1.0 of the corpus probably contained gold morphosyntactic annotations in the XPOS column. These originated from the MULTEXT-East "1984" annotated corpus 4.0 distribution (see the format of these annotations [here](http://nl.ijs.si/ME/V4/msd/html/msd-fa.html)). This XPOS column was probably lost in version 1.1, due to UDPipe re-annotation. In edition 1.3 XPOS was filled in again by a new model of UDPipe but this XPOS content is automatically annotated. It might be interesting to go back to the original MULTIEX-EAST annotations for this column. 
* When upgrading version 1.0 to 1.1, teh LVC category was split into LVC.full and LVC.cause. It might be necessary to manually re-annotated the LVC.cause category, since currently only LVC.full occurs in teh corpus.

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.1 version are the following:
    - updating the morphosyntactic annotations (columns XPOS to DEP_REL) with UDPipe 2, model `persian-perdt-ud-2.10-220711` by Agata Savary
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.
  - The annotated data came from the MULTEXT-East "1984" annotated corpus 4.0 (see https://www.clarin.si/repository/xmlui/handle/11356/1043)
  - The data were adapted in the following way:
    - Adding the verbal multiword expression annotation layer, according to the PARSEME shared task [guidelines version 1.0](https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.0/). 
    - Where necessary, manually annotating part-of-speech tags for consistency with the PARSEME shared task guidelines; notably, all the verbs in VMWEs are annotated with tags started with 'Vl'. These manually tagged morphosyntactic information are provided alongside the data (for further information about the tag-set see http://nl.ijs.si/ME/V4/msd/html/msd-fa.html).


